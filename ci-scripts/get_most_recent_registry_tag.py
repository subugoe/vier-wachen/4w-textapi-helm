from os import getenv
from datetime import datetime
import sys
import json
import requests

BRANCH = sys.argv[1]
GITLAB_TOKEN = getenv('GITLAB_TOKEN')

# get all relevant tags
response = requests.get(
    'https://gitlab.gwdg.de/api/v4/projects/38667/registry/repositories/3377/tags',
    params={'per_page': 100},
    headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
    timeout=10)

tags = []
for entry in json.loads(response.text):
    if BRANCH in entry['name']:
        tags.append(entry['name'])

# get creation dates for relevant tags
tags_w_creation_date = {}
for tag in tags:
    response = requests.get(
        f'https://gitlab.gwdg.de/api/v4/projects/38667/registry/repositories/3377/tags/{tag}',
        headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
        timeout=10)
    response_json = json.loads(response.text)
    creation_date = response_json['created_at']
    tags_w_creation_date[creation_date.split('.')[0]] = tag

# find out the most current tag
creation_dates = [datetime.strptime(date, '%Y-%m-%dT%H:%M:%S') for date in tags_w_creation_date]
now = datetime.now()
most_current = min(creation_dates, key=lambda d: abs(d - now))
most_current_str = datetime.strftime(most_current, '%Y-%m-%dT%H:%M:%S')

print(tags_w_creation_date[most_current_str])
